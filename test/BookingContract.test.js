const contractMock = artifacts.require('BookingContract');

contract('BookingContract', function (accounts) {
  
  
	let mock;
  
	const [
		owner,
		whitelisted_1,
		whitelisted_2,
		anyone,
	] = accounts;

	before(async function () {
		mock = await contractMock.new();
		await mock.addAddressToWhitelist(whitelisted_1, { from: owner });
		await mock.addAddressToWhitelist(whitelisted_2, { from: owner });
	});

	context('in normal conditions', () => {
		
		it('Get all rooms', async function () {
			const value = await mock.getAllRooms({ from: whitelisted_1 });
			assert.equal(value.length, 20, 'Bad number of rooms');
			assert.equal(value[0].substring(2, 8), "433031", 'Bad id');
			assert.equal(value[1].substring(2, 8), "433032", 'Bad id');
		});
		
		it('add and get only one booking for C01', async function () {
			await mock.addBooking("C01", 08, mockSha256("user1@email.com"), { from: whitelisted_1 });
			const value = await mock.getBookings("C01", { from: whitelisted_1 });
			assert.equal(value.length, 1, 'Bad number of bookings');
			assert.equal(value[0], 8, 'Bad timeslot');
		});
		
		it('add and get another booking for C01', async function () {
			await mock.addBooking("C01", 10, mockSha256("user2@email.com"), { from: whitelisted_1 });
			const value = await mock.getBookings("C01", { from: whitelisted_1 });
			assert.equal(value.length, 2, 'Bad number of bookings');
			assert.equal(value[0], 8, 'Bad timeslot');
			assert.equal(value[1], 10, 'Bad timeslot');
		});
		
		it('add and get only one booking for C02', async function () {
			await mock.addBooking("C02", 9, mockSha256("user1@email.com"), { from: whitelisted_2 });
			const value = await mock.getBookings("C02", { from: whitelisted_2 });
			assert.equal(value.length, 1, 'Bad number of bookings');
			assert.equal(value[0], 9, 'Bad timeslot');
		});
		
		it('should cancel a booking and get only one booking for C01', async function () {
			await mock.cancelBooking("C01", 8, mockSha256("user1@email.com"), { from: whitelisted_2 });
			const value = await mock.getBookings("C01", { from: whitelisted_2 });
			assert.equal(value.length, 1, 'Bad number of bookings');
			assert.equal(value[0], 10, 'Bad timeslot');
		});
		
		it('Book the room C02 as user2 and get only one booking', async function () {
			await mock.addBooking("C02", 14, mockSha256("user2@email.com"), { from: whitelisted_1 });
			const value = await mock.getUserBookings("C02", mockSha256("user2@email.com"), { from: whitelisted_1 });
			assert.equal(value.length, 1, 'Bad number of bookings');
			assert.equal(value[0], 14, 'Bad timeslot');
		});
		
		it('Get all bookings of C02', async function () {
			const value = await mock.getBookings("C02", { from: whitelisted_1 });
			assert.equal(value.length, 2, 'Bad number of bookings');
			assert.equal(value[0], 9, 'Bad timeslot');
			assert.equal(value[1], 14, 'Bad timeslot');
		});
		
		it('Anyone can get all bookings', async function () {
			const value = await mock.getBookings("C02", { from: anyone });
			assert.equal(value.length, 2, 'Bad number of bookings');
			assert.equal(value[0], 9, 'Bad timeslot');
			assert.equal(value[1], 14, 'Bad timeslot');
		});
		
		it('The owner can clear all bookings', async function () {
			await mock.clearAllBookings({ from: owner });
			const bookingsC01 = await mock.getBookings("C01", { from: owner });
			assert.equal(bookingsC01.length, 0, 'Bad number of bookings');
			const bookingsC02 = await mock.getBookings("C02", { from: owner });
			assert.equal(bookingsC02.length, 0, 'Bad number of bookings');
		});
	});
	
	context('error cases', () => {
		it('Anyone can not book a room', async function () {
			await tryCatch(mock.addBooking("C01", 08, mockSha256("user3@email.com"), { from: anyone }), "revert");
		});
		
		it('Only the owner can clear all bookings', async function () {
			await tryCatch(mock.clearAllBookings({ from: anyone }), "revert");
			await tryCatch(mock.clearAllBookings({ from: whitelisted_1 }), "revert");
			await tryCatch(mock.clearAllBookings({ from: whitelisted_2 }), "revert");
		});
		
		it('Can not book a room already booked', async function () {
			await mock.addBooking("C01", 8, mockSha256("user1@email.com"), { from: whitelisted_1 });
			
			await tryCatch(mock.addBooking("C01", 8, mockSha256("user1@email.com"), { from: whitelisted_1 }), "revert");
			await tryCatch(mock.addBooking("C01", 8, mockSha256("user2@email.com"), { from: whitelisted_2 }), "revert");
		});
		
		it('Order a booking out of range', async function () {
			await tryCatch(mock.addBooking("C03", -1, mockSha256("user1@email.com"), { from: whitelisted_1 }), "revert");
			await tryCatch(mock.addBooking("C03", 24, mockSha256("user1@email.com"), { from: whitelisted_1 }), "revert");
		});
		
		it('Book an unknown room', async function () {
			await tryCatch(mock.addBooking("C33", 8, mockSha256("user1@email.com"), { from: whitelisted_1 }), "revert");
		});
	});
		
	function mockSha256(roomId){
		switch(roomId){
			case "user1@email.com" : return "0x3cf1fabc408e09d2e0a44069c0946bfbcfcffd5031174c712a4f3128bf7964bc";
			case "user2@email.com" : return "0x414f3af70029459e6a1379bc5312174a55f50818bc5b403fe84a3fb4c583415a";
			case "user3@email.com" : return "0x8c09587a29709e9793f8e4281e2248500ccf9ce64835400c637a3678db619796";
			case "user4@email.com" : return "0xfd8827a29dc4320b11c2d5f7d0fb79d9a5e9cc418dc4a672f9d2e7e53f9f1e6c";
			case "user5@email.com" : return "0xc7928fe445c23719cffe2eb1fbf4791200ad2a1bd696a38e3b675ac1785559f6";
			default : return "0xaf6306bc5d6ac9a53bf0331ba1c862511486562cf10c9cd67fdfda41b7fc3177";
		}
	}
	
	const PREFIX = "VM Exception while processing transaction: ";

	async function tryCatch(promise, message) {
		try {
			await promise;
			throw null;
		}
		catch (error) {
			assert(error, "Expected an error but did not get one");
			assert(error.message.startsWith(PREFIX + message), "Expected an error starting with '" + PREFIX + message + "' but got '" + error.message + "' instead");
		}
	};
});