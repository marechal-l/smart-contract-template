# smart-contract-template

Generated using [Truffle Box](https://truffle-box.github.io/) 

## Getting Started

This repository contains a smart contract and some tests

### Prerequisites

* Node JS installed
* A running etherum network (Ganache for development purpose)

### Installing

Download dependencies

```
npm install
```

### Interacting with the smart contract

Use the following commands to compile, test and deploy a smart contract.

These commands works only if you are a working rpc listenning on http://127.0.0.1:8545/, if not, see the next section.

```
truffle compile
truffle test
truffle migrate
```

You can now extract the **contract address** from the last command and the **abi** from build/contracts/SampleContract.json in order to use it in a decentralized application.
See https://gitlab.com/marechal-l/web-dapp-template for example.

## Running on a test rpc

This command will run a test rpc on http://127.0.0.1:9545/

```
truffle develop --log &
```

Then run the tests on this network using the networks defined in truffle-config.json

```
truffle test --network test
```